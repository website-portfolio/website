# Website

This is the website backend of T.D.J. Rijpkema, and on this website I am showing of some projects that I have
done over the years. The website is split up in three main sections:


- Projects: This displays a set of projects I have done, together with source code (if available).
- Blog: In this section are blogs about projects, which can be used as a series to learn more about a certain subject.


## Running the project

I have used JetBrains IntelliJ IDEA, and cloned the project from gitlab.
Alternatively, you can download the projects as a zip file and open it in any IDE.
Then you require npm (alternatively yarn) to install all the packages and run the website

```bash
npm install
npm run dev
```

### Build

In order to get the static files you can use:

```bash
npm run build
```



> Tested with npm version 8.19.1